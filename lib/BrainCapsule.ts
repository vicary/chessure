import type { ConditionalExcept, Merge } from "type-fest";
import type { FrozenBrain } from "./FrozenBrain.ts";

/**
 * Brain kept into a transport capsule, to be sent between workers and/or main
 * process.
 */
export type BrainCapsule = Merge<
  ConditionalExcept<FrozenBrain, (...args: any[]) => any>,
  {
    mutationRate?: number;
    weights: ArrayBufferLike;
  }
>;
