export type CloneOptions = { crossover?: Uint8Array };

export interface Cloneable {
  clone(options?: CloneOptions): Promise<this>;
}
