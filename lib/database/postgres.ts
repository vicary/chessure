import { assert } from "assert";
import { config } from "dotenv";
import { Pool } from "postgres";

const { DATABASE_CONNECTION_URL } = config();
assert(
  DATABASE_CONNECTION_URL,
  "Invalid application environment, required variable(s): DATABASE_CONNECTION_URL.",
);

const pool = new Pool(DATABASE_CONNECTION_URL, 10, true);

export const createConnection = () => pool.connect();
