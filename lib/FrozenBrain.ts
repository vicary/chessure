import type { io } from "@tensorflow/tfjs";
import { assert } from "assert";
import { transfer } from "comlink";
import { customAlphabet, urlAlphabet } from "nanoid";
import { BrainCapsule } from "./BrainCapsule.ts";

export const autoId = (size: number) =>
  customAlphabet(urlAlphabet.replace(/[-_]/g, ""), size)();

/**
 * Frozen brains to be preserved in the cloud.
 */
export class FrozenBrain {
  constructor({
    id = autoId(8),
    timestamp = new Date(),
    bestScore = 0,
    thisScore = 0,
    model,
    weights,
  }: Partial<FrozenBrain> = {}) {
    this.id = id;
    this.timestamp = timestamp;
    this.bestScore = bestScore;
    this.thisScore = thisScore;

    assert(model && weights, "model and weights are required");

    this.model = model;
    this.weights = weights;
  }

  id: string;
  timestamp: Date;
  bestScore: number;
  thisScore: number;
  model: io.ModelArtifacts;
  weights: Uint8Array;

  encapsulate(): BrainCapsule {
    const brainCapsule: BrainCapsule = {
      ...this,
      weights: this.weights.buffer,
    };

    return transfer(brainCapsule, [brainCapsule.weights]);
  }

  static decapsulate({ weights, ...brain }: BrainCapsule) {
    return new FrozenBrain({ ...brain, weights: new Uint8Array(weights) });
  }
}
