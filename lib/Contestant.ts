export interface Contestant {
  bestScore?: number;
  thisScore?: number;
}
