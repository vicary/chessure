import * as tf from "@tensorflow/tfjs";
import { assert } from "assert";
import { autoId, FrozenBrain } from "../FrozenBrain.ts";

const MUTATION_VARIATION = 1;

type BrainMetadata = {
  name: string;
};

/**
 * Brians with a brain.
 */
export class Brian {
  constructor(readonly brain: tf.LayersModel) {
    const data = this.brain.getUserDefinedMetadata() as
      | BrainMetadata
      | undefined;
    if (!data?.name) {
      this.brain.setUserDefinedMetadata({ name: autoId(8) });
    }
  }

  get metadata() {
    return this.brain.getUserDefinedMetadata() as BrainMetadata;
  }

  set metadata(value: BrainMetadata) {
    this.brain.setUserDefinedMetadata(value);
  }

  get name() {
    return this.metadata.name;
  }

  set name(value: string) {
    this.metadata = { name: value };
  }

  think(input: tf.Tensor | tf.Tensor[]) {
    return tf.tidy(() => this.brain.predict(input, { batchSize: 1 }));
  }

  /**
   * Blast this brian with a healthy(?) dose of radiation.
   */
  mutate(rate: number, { verbose = false }: { verbose?: boolean } = {}) {
    if (rate <= 0) return this;

    const perfHash = autoId(4);
    if (verbose) console.time(`[mut] ${perfHash}`);

    tf.tidy(() => {
      const newWeights = this.brain.getWeights().map((weight, i) => {
        // console.timeLog(`perf-mut#${perfHash}`, i);
        const binWeights = weight.dataSync().slice();
        const mutWeights = tf.randomStandardNormal(weight.shape).dataSync();
        for (let i = 0; i < binWeights.length; i++) {
          if (Math.random() < rate) {
            binWeights[i] += mutWeights[i] * MUTATION_VARIATION;
          }
        }

        return tf.tensor(binWeights, weight.shape);
      });

      this.brain.setWeights(newWeights);
    });
    if (verbose) console.timeEnd(`[mut] ${perfHash}`);

    this.name = `${this.name.split("#").shift() ?? autoId(8)}#${autoId(8)}`;

    return this;
  }

  /**
   * Produce a new offspring with the given mate.
   */
  async mate(
    /** The mating Brain, preferably with a brain. */
    mate: Brian,
    /** Probability to be replaced with the incoming gene. */
    rate: number = Math.random(),
  ): Promise<typeof this> {
    const { model } = await this.freeze();
    const brain = await tf.loadLayersModel({
      load: () => Promise.resolve(model),
    });

    tf.tidy(() => {
      const matWeights = mate.brain.getWeights();
      const newWeights = this.brain.getWeights().map((weight, i) => {
        if (Math.random() < rate) {
          return matWeights[i].clone();
        } else {
          return weight.clone();
        }
      });

      brain.setWeights(newWeights);
    });

    const brian = new (this.constructor as any)(brain);

    const thisFamily = this.name.split("#").shift() ?? autoId(4);
    const thatFamily = mate.name.split("#").shift() ?? autoId(4);

    brian.name =
      [...new Set([...`${thisFamily}-${thatFamily}`.split("-").sort()])].join(
        "-",
      ) + `#${autoId(4)}`;

    return brian;
  }

  static async defrost({ id = autoId(8), model, weights }: FrozenBrain) {
    const brain = await tf.loadLayersModel({
      load: () => Promise.resolve({ ...model, weightData: weights }),
    });
    const brian = new Brian(brain);

    brian.name = id;

    return brian;
  }

  async freeze() {
    const { weightData: weights, ...model } = await new Promise<
      tf.io.ModelArtifacts
    >((resolve) =>
      this.brain.save(
        tf.io.withSaveHandler((model) => {
          resolve(model);

          return Promise.resolve({
            modelArtifactsInfo: {
              dateSaved: new Date(),
              modelTopologyType: "JSON",
            },
          });
        }),
      )
    );

    assert(weights, "Expected `weightData` from the brain model.");

    return new FrozenBrain({
      id: this.name,
      model,
      weights: new Uint8Array(weights),
    });
  }

  dispose() {
    this.brain.dispose();
  }
}
