import * as tf from "@tensorflow/tfjs";
import { assert } from "assert";
import type { ChessInstance, PieceColor, ShortMove, Square } from "chess.js";
import { transfer } from "comlink";
import { BrainCapsule } from "../BrainCapsule.ts";
import { Contestant } from "../Contestant.ts";
import { createConnection } from "../database/postgres.ts";
import { autoId, FrozenBrain } from "../FrozenBrain.ts";
import { Brian } from "./Brian.ts";

type Board = ReturnType<ChessInstance["board"]>;

/** A special error to designate resignation. */
export class ResignError extends Error {}

// 8x8x12 inputs -> X relu layers -> 64x64x5 output (softmax)
export const createRandomBrain = (
  { verbose = false }: { verbose?: boolean } = {},
) => {
  const perfHash = autoId(4);
  if (verbose) console.time(`[new] ${perfHash}`);

  const brain = tf.sequential();

  brain.add(tf.layers.inputLayer({ inputShape: [8, 8, 12] }));
  brain.add(tf.layers.flatten());

  // decision layers (movements & promotions)
  // let movementComplexity = Math.round(Math.random() * 5 + 1);
  let movementComplexity = 8;
  while (movementComplexity-- > 0) {
    brain.add(
      tf.layers.dense({
        activation: "relu",
        units: 8 * 8 * 12,
        kernelInitializer: "randomUniform",
        useBias: false,
        trainable: false,
      }),
    );
  }

  brain.add(
    tf.layers.dense({
      activation: "softmax",
      units: 64 * 64 * 6,
      kernelInitializer: "randomUniform",
      useBias: false,
      trainable: false,
    }),
  );

  brain.add(tf.layers.reshape({ targetShape: [64, 64, 6] }));

  if (verbose) console.timeEnd(`[new] ${perfHash}`);

  return brain;
};

/**
 * A Brian that understands Chess.js.
 */
export class ChessBrian extends Brian implements Contestant {
  bestScore = 0;
  thisScore = 0;

  constructor(brain: tf.LayersModel = createRandomBrain()) {
    super(brain);
  }

  /**
   * Ask ChessBrian to think of a move.
   */
  think(input: tf.Tensor | tf.Tensor[]): tf.Tensor | tf.Tensor[];
  think(chess: ChessInstance): Promise<ShortMove>;
  think(input: tf.Tensor | tf.Tensor[] | ChessInstance) {
    if (Array.isArray(input) || input instanceof tf.Tensor) {
      const result = super.think(input);

      return result;
    } else {
      const output = tf.tidy(() => {
        const board = this.#encodeBoard(input.board(), input.turn());
        const output = super.think(board);

        assert(isOutputTensor(output));

        return tf.squeeze(output) as tf.Tensor<tf.Rank.R3>;
      });

      const move = this.#decodeMove(output, input.turn()).finally(() =>
        output.dispose()
      );

      return move;
    }
  }

  async freeze() {
    const frozenBrain = await super.freeze();

    frozenBrain.bestScore = this.bestScore;
    frozenBrain.thisScore = this.thisScore;

    return frozenBrain;
  }

  static async defrost(
    { id = autoId(4), model, weights, bestScore = 0 }: FrozenBrain,
  ) {
    return await this.decapsulate({
      id,
      bestScore,
      model,
      weights: weights.buffer,
    });
  }

  async encapsulate(): Promise<BrainCapsule> {
    const { weights, ...brain } = await this.freeze();
    const brainCapsule = { ...brain, weights: weights.buffer };

    return transfer(brainCapsule, [brainCapsule.weights]);
  }

  static async decapsulate(
    { id, bestScore, model, weights }: Omit<
      BrainCapsule,
      "thisScore" | "timestamp"
    >,
  ) {
    const brain = await tf.loadLayersModel({
      load: () => Promise.resolve({ ...model, weightData: weights }),
    });
    const brian = new ChessBrian(brain);

    brian.name = id;
    brian.bestScore = bestScore;

    return brian;
  }

  /**
   * Extract model and weights, uploads to database.
   */
  async upload(): Promise<FrozenBrain> {
    const client = await createConnection();
    const {
      rows: [brianStorage],
    } = await client.queryObject<FrozenBrain>(
      `SELECT id, best_score FROM frozen_brain WHERE id = $id`,
      {
        id: this.name,
      },
    );

    if (brianStorage) {
      if (brianStorage.bestScore !== this.bestScore) {
        brianStorage.bestScore = this.bestScore;
        const { rows } = await client.queryObject<FrozenBrain>(
          `UPDATE frozen_brain SET bestSscore = $bestScore WHERE id = $id RETURNING *`,
          { ...brianStorage },
        );
        return rows[0];
      }

      return brianStorage;
    } else {
      const { weights, ...model } = await this.freeze();
      assert(weights, `[${this.name}] Expected weight data from Brian.`);

      const {
        rows: [brianStorage],
      } = await client.queryObject<FrozenBrain>(
        `INSERT INTO frozen_brain (id, best_score, model) VALUES ($id, $bestScore, $model) RETURNING *`,
        {
          id: this.name,
          bestScore: this.bestScore,
          model,
          weights,
        },
      );

      client.release();

      return brianStorage;
    }
  }

  /**
   * A [8, 8, 12] tensor, representing the board with each type of pieces.
   *
   * Instead of black/white, the board is encoded as player/opponent.
   *
   * For black side, the board will be flipped.
   *
   * Pieces are encoded like this:
   * - 0: my pawn
   * - 1: my knight
   * - 2: my bishop
   * - 3: my rook
   * - 4: milady
   * - 5: my king
   * - 6: their pawn
   * - 7: their knight
   * - 8: their bishop
   * - 9: their rook
   * - 10: bitch
   * - 11: their king
   */
  #encodeBoard(board: Board, side: PieceColor) {
    const values: number[][][] = [];

    if (side === "b") {
      for (let rank = 7; rank >= 0; rank--) {
        const row: number[][] = [];
        for (let file = 7; file >= 0; file--) {
          const square = board[rank][file];
          const pieces: number[] = new Array(12).fill(0);

          if (square !== null) {
            pieces[
              ["p", "n", "b", "r", "q", "k"].indexOf(square.type) +
              (square.color === side ? 6 : 0)
            ] = 1;
          }

          row.push(pieces);
        }
        values.push(row);
      }
    } else {
      for (let rank = 0; rank < 8; rank++) {
        const row: number[][] = [];
        for (let file = 0; file < 8; file++) {
          const square = board[rank][file];
          const pieces: number[] = new Array(12).fill(0);

          if (square !== null) {
            pieces[
              ["p", "n", "b", "r", "q", "k"].indexOf(square.type) +
              (square.color === side ? 6 : 0)
            ] = 1;
          }

          row.push(pieces);
        }
        values.push(row);
      }
    }

    return tf.tensor4d([values], [1, 8, 8, 12]);
  }

  /**
   * Output tensor should be a [64, 64, 5] tensor.
   *
   * D1: From X,Y
   * D2: To X,Y
   * D3: 0=non-capture move, 1=knight promotion, 2=bishop promotion, 3=rook promotion, 4=queen promotion
   */
  async #decodeMove(
    tensor: tf.Tensor<tf.Rank.R3>,
    side: PieceColor,
  ): Promise<ShortMove> {
    const moveTensor = await tensor.array();

    assert(moveTensor.length === 64, "Expected 64 dimensions `from`");
    assert(
      moveTensor.every((rank) => rank.length === 64),
      "Expected 64 dimensions `to`",
    );
    assert(
      !moveTensor.flat(3).every((value) => value === 0),
      "Unexpected all-zeros from a softmax tensor.",
    );

    let maxPt = { src: 0, dst: 0, modifier: 0, value: -Infinity };
    for (let src = 0; src < 64; src++) {
      for (let dst = 0; dst < 64; dst++) {
        for (let modifier = 0; modifier < 5; modifier++) {
          const value = moveTensor[src][dst][modifier];
          if (value > maxPt?.value) {
            maxPt = { src: src, dst: dst, modifier, value };
          }
        }
      }
    }

    assert(isFinite(maxPt.value), "Expected a finite value for the move.");

    if (maxPt.modifier === 5) {
      throw new ResignError();
    }

    return {
      from: this.#decodeSquare(maxPt.src, side),
      to: this.#decodeSquare(maxPt.dst, side),
      // TODO: chess.js will tolerate invalid promotion values, correctly train this when promotions actually happen.
      promotion: [
        "n",
        "b",
        "r",
        "q",
      ][maxPt.modifier - 1] as ShortMove["promotion"],
    };
  }

  #decodeSquare(index: number, side: PieceColor): Square {
    assert(index >= 0 && index < 64);
    const file = (side === "b" ? "hgfedcba" : "abcdefgh").charAt(index / 8);
    const rank = side === "b" ? 8 - (index % 8) : (index % 8) + 1;

    return `${file}${rank}` as Square;
  }
}

const isOutputTensor = (value: tf.Tensor | tf.Tensor[]): value is tf.Tensor =>
  !Array.isArray(value) && value.shape.length === 4;
