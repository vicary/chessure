import * as tf from "@tensorflow/tfjs";
import "@tensorflow/tfjs-backend-cpu";
import "@tensorflow/tfjs-backend-webgpu";
import { assert } from "assert";
import type { ChessInstance } from "chess.js";
import { Chess } from "chess.js";
import { expose } from "comlink";
import type { Executable } from "workerpool";
import { genChessGame } from "../commands/train/genChessGame.ts";
import { updateFitness } from "../commands/train/updateFitness.ts";
import { BrainCapsule } from "../lib/BrainCapsule.ts";
import { ChessBrian, createRandomBrain } from "../lib/brians/ChessBrian.ts";

await tf.setBackend("cpu");

/**
 * For player1 and player2,
 * 1. Omit either one or both for new random brains.
 * 2. Specify a brain without mutationRate to use it as is.
 * 3. Specify a brain and a mutationRate to mutate it before use.
 *
 * For games, exactly two games will be played.
 * 1. The first game will have player1 as white.
 * 2. The second game will have player2 as white.
 * 3. Provide the games array with a fen string to play in a specific board configuration.
 */
export type ChessGamePayload = {
  /** Seat position of this tournament. */
  seat: number;

  brain1?: BrainCapsule;
  brain2?: BrainCapsule;
  fen?: string;
  verbose?: number;
};

export type ChessGameResult = {
  brains: [BrainCapsule, BrainCapsule];
  games: string[];
  seat: number;
};

export type ExecutableChessGame = Executable<ChessGamePayload, ChessGameResult>;

const ChessConstructor = Chess as unknown as {
  new (fen?: string): ChessInstance;
};

export class ChessGame implements ExecutableChessGame {
  async execute({ brain1, brain2, fen, seat, verbose = 0 }: ChessGamePayload) {
    const [player1, player2] = await Promise.all(
      [brain1, brain2].map((b) => brainToBrian(b, { verbose: verbose >= 2 })),
    );

    player1.bestScore *= 0.8;
    player2.bestScore *= 0.8;

    const game1 = new ChessConstructor(fen);
    for await (
      const move of genChessGame({
        game: game1,
        black: player1,
        white: player2,
        verbose: verbose >= 3,
      })
    ) {
      updateFitness(move);
    }

    const game2 = new ChessConstructor(fen);
    for await (
      const move of genChessGame({
        game: game2,
        black: player2,
        white: player1,
        verbose: verbose >= 3,
      })
    ) {
      updateFitness(move);
    }

    player1.thisScore /= 2;
    if (player1.thisScore > player1.bestScore) {
      player1.bestScore = player1.thisScore;
    }

    player2.thisScore /= 2;
    if (player2.thisScore > player2.bestScore) {
      player2.bestScore = player2.thisScore;
    }

    if (player1.bestScore < 0.01) {
      player1.bestScore = 0;
    }

    if (player2.bestScore < 0.01) {
      player2.bestScore = 0;
    }

    const brains =
      (await Promise.all([player1, player2].map(brianToBrain))) as [
        BrainCapsule,
        BrainCapsule,
      ];

    return {
      brains,
      games: [game1.pgn(), game2.pgn()],
      seat,
    };
  }
}

const brainToBrian = async (
  brain?: BrainCapsule,
  { verbose = false }: { verbose?: boolean } = {},
) => {
  if (!brain) {
    return new ChessBrian(createRandomBrain({ verbose }));
  } else {
    assert(brain.model && brain.weights);

    const brian = await ChessBrian.decapsulate(brain);
    const mutationRate = brain.mutationRate ?? 0;
    if (mutationRate > 0) {
      brian.mutate(mutationRate, { verbose });
    }

    return brian;
  }
};

const brianToBrain = async (brian: ChessBrian): Promise<BrainCapsule> => {
  const brainCapsule = await brian.encapsulate();

  brian.dispose();

  return brainCapsule;
};

expose(new ChessGame());
