# Chessure

A treasure chest for Chess.

## Current focus

My current focus is to build a public available deep learning models of chess
bots, various play style and skill cap will be kept for educational purpose.

The next milestone is to setup a website where chess players can practise with
any level of bot with drastically different play styles.

The ultimate goal is to let everyone build professional chess games without
having to buy or write bot engines, handcrafted brute-force bots are so last
century.

### Roadmap

Currently this is in the middle of a remake, the project will be completely
ported to Deno in the foreseeable future.

1. [ ] Headless Trainer: This is almost done, actual training is distributed to
       web workers, spreading the load across all available cores.
1. [ ] User Interface: The react codebase will be ported into Deno's fresh
       framework, and deloyed via `deno deploy` to leverage their beautiful edge
       servers.

### General Ideas

These are general ideas which will ultimately becomes actionables in the
Roadmap.

#### 1. Allow illegal moves

Currently making illegal moves immediately loses a game.

The goal is to allow illegal moves, and the opponent may challenge it when it
happens.

1. A successful challenge undo the illegal move, adds 2 minutes to the clock,
   and the move is recorded.
2. A second successful challenge to an illegal move wins the game.
3. A challenge against a legal move adds 2 minutes to the clock of the player
   being challenged.
4. Capturing the King immediately forfeits the game, no challenge is required.

Supplementary UI must be in place to help prevent mouseslip, e.g. snap to grid
when dragging.

#### 2. Clocks

1. With network latency, clocks maybe skewed and must be counted in servers.
2. Clocks must be synchronized on every other move, when it's the current
   players turn.
3. Clock ticks are synchronized to every 100ms.
4. For bot players, the runtime box must be resource-constrained to mimic a
   reasonable thinking time.
5. Models will be clock-aware, encouraging short-circuit paths when time is low.

#### 3. Coins circulation control

1. In game tokens are minted the moment when player adds fund.
2. AI players will join the games, they win tokens from normal players.
3. If a bot player is strong enough, tokens are essentially locked away from
   circulation.
4. Strong players may try to unlock tokens by challenging strong bots.
5. When circulating tokens go above a certain level, tokens won by bots are
   immediately burned.

#### 4. Resiging bots

1. Bots can resign when facing strong opponents, saves time to work on lesser
   players.
2. This requires another strategic model layered on top of the chess playing
   model.

#### 5. Species diversity

1. Incubation should be in place. For each epoch, an even number of new random
   players are always generated and played against each other.
2. At most, 3 spieces will be kept at each time.
3. Spieces with fitness scores within a 10% difference will be matched against
   each others.

## Why Tensorflow.js on Deno?

Because TypeScript is the most approachable language, strikes the best balance
in performance and reaches the most of you. I want all of you juniors find fun
by toying around my codes here.

[Also Python is hella slow](https://github.com/kostya/benchmarks), and Workers
in Deno is the easiest way to achieve multithreading.

Feel free to download and try the models. Let me know what do you think,
constructive criticism is always welcomed.

## Will code for food

If you think I did a good job, [keep me fed](https://github.com/sponsors/vicary)
or I'll literally starve.
