import type { Command } from "commander";
import { cpus } from "os";
import type { FrozenBrain } from "../lib/FrozenBrain.ts";
import { downloadBestBrains } from "./train/downloadBestBrains.ts";
import { matchPlayers } from "./train/matchPlayers.ts";
import { createGamingPool } from "./train/workerpools/gaming.ts";

const SEEDING_RATIO = 0.2;

export const train = async ({
  concurrency,
  fitnessThreshold,
  fixedMutationRate = false,
  generations: maxGeneration,
  mutationRate: maxMutationRate,
  population: maxPopulation,
  resume = false,
  verbose = 0,
}: {
  concurrency: number;
  fitnessThreshold?: number;
  fixedMutationRate?: boolean;
  generations: number;
  mutationRate: number;
  population: number;
  resume?: boolean | string[];
  verbose?: number;
}) => {
  const pool = createGamingPool({
    concurrency,
    fitnessThreshold,
    fixedMutationRate,
    generations: maxGeneration,
    mutationRate: maxMutationRate,
    population: maxPopulation,
    reportingInterval: Math.max(1, Math.round(maxGeneration * 0.01)), // Log every 1%
    verbose,
  });

  {
    // Seeding phase, kickstart the workerpool cycle.
    const seeds: Array<FrozenBrain | undefined> =
      resume === false
        ? []
        : await downloadBestBrains(
            resume === true ? Math.ceil(maxPopulation * SEEDING_RATIO) : Array.isArray(resume) ? resume : [resume],
          );

    console.info(
      `Seeding... 🤖${maxPopulation} 🌱${seeds.length} 🥇${Math.max(0, ...seeds.map((b) => b?.bestScore ?? 0))}${
        fitnessThreshold ? `>=${fitnessThreshold}` : ""
      } 👪${maxGeneration}`,
      seeds.map((b) => b?.id.trim()).filter(Boolean),
    );

    seeds.push(...new Array(maxPopulation - seeds.length).fill(undefined));

    matchPlayers(seeds).forEach(([brain1, brain2], seat) => {
      pool.enqueue({
        name: "ChessGame",
        payload: {
          brain1: brain1?.encapsulate(),
          brain2: brain2?.encapsulate(),
          seat,
          verbose,
        },
      });
    });

    pool.start();
  }
};

const addTrainCommand = (program: Command) =>
  program
    .option<string[] | boolean>(
      "-r, --resume [brian...]",
      "Download named brains and resume their training, defaults to the best brains in database.",
      (value, previous) => {
        if (previous === true) {
          throw new Error(`???`);
        } else if (previous === false) {
          return value ? [value] : true;
        } else {
          return [...previous, value];
        }
      },
      false,
    )
    .option("-f, --fixed-mutation-rate", "Use a fixed mutation rate instead of linear decay.")
    .option(
      "-m, --mutation-rate <double>",
      // DOI: 10.1109/APS.2000.875398
      "Mutation rate, linearly interpolates into 1/ℓ unless --fixed-mutaiton-rate is specified.",
      (value: string) => parseFloat(value),
      0.5,
    )
    .option("-p, --population <number>", "Target population size.", (value: string) => parseInt(value), 20)
    .option(
      "-g, --generations <number>",
      "Generations to run, also used for dynamic mutation rate.",
      (value: string) => parseInt(value),
      5_000,
    )
    .option(
      "-A, --fitness-threshold <number>",
      "For each generation, fitness lower than this number will be discarded. On termination, brains with this fitness or higher will be uploaded.",
      (value: string) => parseInt(value),
    )
    .option(
      "-c, --concurrency <number>",
      "Concurrent workers for games, defaults to the number of CPUs minus one.",
      (value: string) => parseInt(value),
      cpus().length - 1,
    )
    .option("-v, --verbose <number>", "Enable verbose logging.", (value: string) => parseInt(value), 0)
    .action(train);

export default addTrainCommand;
