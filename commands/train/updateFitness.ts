import type { ChessBrian } from "../../lib/brians/ChessBrian.ts";
import type { ChessGameMove } from "./genChessGame.ts";

const checkCounts = new WeakMap<ChessBrian, number>();

export const updateFitness = (
  { captured, opponent, player, flags, turn }: ChessGameMove,
) => {
  const checkCount = checkCounts.get(player) ?? 0;

  // Illegal moves
  if (flags.includes("I")) {
    // player.thisScore -= 30;
    return;
  } // 1. First 10 legal moves are rewarded inversely. 1st=10, 2nd=9, 3rd=8, etc.
  else {
    if (turn <= 10) {
      player.thisScore += 10 - turn;
    }
  }

  // 2. Check attempts should be rewarded with a diminishing reward. 1st=3, 2nd=2, 3rd=1.
  if (flags.includes("C")) {
    player.thisScore += Math.max(0, 3 - checkCount);

    checkCounts.set(player, checkCount + 1);
  }

  // 5. A stalemate scores 0 points, but captured pieces counts.
  // 6. Capturing a piece scores its standard weights: p = 1, n = 3, b = 3, r = 5, q = 9.
  if (captured === "p") {
    player.thisScore += 1;
  } else if (captured === "n" || captured === "b") {
    player.thisScore += 3;
  } else if (captured === "r") {
    player.thisScore += 5;
  } else if (captured === "q") {
    player.thisScore += 9;
  }

  // Reward en passants.
  if (flags.includes("e")) {
    player.thisScore += 1;
  }

  // 3. A checkmate scores 50 points
  // 4. A checkmate within 10 turns adds all the remaining first-10 legal move scores.
  if (flags.includes("M")) {
    console.info("Checkmate!!!", player.name);
    player.thisScore += 50 + Math.max(0, 10 - turn);
  } // 7. Dragging on reduces fitness.
  else if (flags.includes("T")) {
    player.thisScore -= 5;
  }

  // 8. Dragging on reduces fitness, indefinitely.
  if (turn >= 50) {
    player.thisScore--;
    opponent.thisScore--;
  }
};
