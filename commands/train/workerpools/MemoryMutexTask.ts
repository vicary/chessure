import type { Task } from "workerpool";

export type MemoryMutexTask<TPayload> = Task<TPayload> & { busy?: boolean };
