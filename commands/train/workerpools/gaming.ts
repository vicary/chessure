import { assert } from "assert";
import * as colors from "colors";
import { ExecutableWorker, Workerpool } from "workerpool";
import { BrainCapsule } from "../../../lib/BrainCapsule.ts";
import { FrozenBrain } from "../../../lib/FrozenBrain.ts";
import {
  ChessGamePayload,
  ChessGameResult,
} from "../../../workers/ChessGame.ts";
import { equalityCloning } from "../breedingStrategies/equalityCloning.ts";
import { equalityMating } from "../breedingStrategies/equalityMating.ts";
import { matchPlayers } from "../matchPlayers.ts";
import { eugenics } from "../selectionStrategies/eugenics.ts";
import { uploadFrozenBrains } from "../uploadFrozenBrains.ts";
import { verboseGames } from "../verboseGames.ts";
import { verboseSeeds } from "../verboseSeeds.ts";
import { MemoryMutexTask } from "./MemoryMutexTask.ts";

class ChessGame extends ExecutableWorker<ChessGamePayload, ChessGameResult> {
  constructor() {
    super(new URL("../../../workers/ChessGame.ts", import.meta.url).href);
  }
  onFailure(error: Error) {
    console.error(error);

    return Promise.resolve(false);
  }
}

export type GamingPoolOptions = {
  concurrency: number;
  fitnessThreshold?: number;
  fixedMutationRate?: boolean;
  generations: number;
  mutationRate: number;
  population: number;
  reportingInterval?: number;
  selectionRatio?: number;
  verbose?: number;
};

const lerp = (a: number, b: number, t: number) => a * (1 - t) + b * t;

const countDigits = (n: number) => (Math.log(n) * Math.LOG10E + 1) | 0;

export const createGamingPool = ({
  concurrency,
  fitnessThreshold,
  fixedMutationRate = false,
  generations: maxGeneration,
  mutationRate: maxMutationRate,
  population: maxPopulation,
  reportingInterval = 0,
  selectionRatio = 0.4,
  verbose = 0,
}: GamingPoolOptions) => {
  const queue = new Set<MemoryMutexTask<ChessGamePayload>>();
  const brains = new Set<BrainCapsule>();
  const games = new Map<number, string[]>();

  let generation = 0;
  let overallBest = 0;

  const pool = new Workerpool<ChessGamePayload, ChessGameResult>({
    concurrency,
    workers: [ChessGame],
    enqueue(task: MemoryMutexTask<ChessGamePayload>) {
      task.busy = false;
      queue.add(task);
    },
    dequeue() {
      for (const task of queue) {
        if (!task.busy) {
          task.busy = true;
          return task;
        }
      }
    },
    onTaskFinished(error, result, { task }) {
      queue.delete(task as MemoryMutexTask<ChessGamePayload>);

      if (error) throw error;
      assert(result);

      for (const brain of result.brains) {
        brains.add(brain);
      }

      games.set(result.seat, result.games);
    },
    async onStateChange(state) {
      if (state === "drained") {
        if (brains.size === 0) {
          pool.pause();
          return;
        }

        overallBest = Math.max(
          overallBest,
          ...[...brains].map((b) => Number(b.bestScore ?? 0)),
        );

        if (overallBest > (fitnessThreshold ?? 0)) {
          generation++;
        }

        // Armageddon, ascend fit brains.
        if (generation >= maxGeneration) {
          // Because best score now decays, we have to pick current best instead of overall best.
          const currentBest = Math.max(
            ...[...brains].map((b) => Number(b.bestScore ?? 0)),
          );

          const ascendingBrains = overallBest > 0
            ? [...brains]
              .filter((b) =>
                (b.bestScore ?? 0) >= (fitnessThreshold ?? currentBest)
              )
              .map((capsule) => FrozenBrain.decapsulate(capsule))
            : [];

          games.clear();
          brains.clear();

          pool.pause();

          setTimeout(
            (brains: FrozenBrain[]) => {
              uploadFrozenBrains(brains);

              console.info(
                colors.red(
                  `Armageddon, 💾 🤖${brains.length}.\n${
                    brains.map((b, i) => `${i + 1}. ${b.id}`).join("\n")
                  }`,
                ),
              );
            },
            0,
            ascendingBrains,
          );

          return;
        }

        if (verbose >= 1) {
          const table = verboseGames(games);
          if (table.getRows().length > 0) {
            console.debug(table.toString());
          }

          const fitness = verboseSeeds([...brains]);
          if (fitness.length > 0) {
            console.debug(colors.gray(fitness));
          }
        }

        games.clear();

        const brainCapsules: Array<BrainCapsule | undefined> = [];

        if (overallBest > 0) {
          const mutationRate = fixedMutationRate ? maxMutationRate : lerp(
            maxMutationRate,
            1 / maxPopulation,
            generation / maxGeneration,
          );

          // eugenics is now only a seed picker, breeding will be handled by workers.
          const seeds = eugenics({
            // fitnessThreshold,
            population: [...brains],
            selectionRatio,
          }).filter(({ bestScore, thisScore }) =>
            bestScore > 0 || thisScore > 0
          );

          brainCapsules.push(...seeds);

          // Forces 5% random population, at least 1.
          {
            const randomPopulation = Math.max(
              1,
              Math.floor(maxPopulation * 0.05),
            );
            brainCapsules.push(...new Array(randomPopulation).fill(undefined));
          }

          // Crossbreed for half of the target population.
          if (seeds.length >= 2 && seeds.length < maxPopulation) {
            const targetPopulation = Math.max(
              0,
              Math.round((maxPopulation * selectionRatio) / 2),
            );
            if (targetPopulation > 0) {
              const offsprings = await equalityMating(
                // We use all brains with positive scores, bringing in new moves from new blood.
                [...brains].filter((b) => b.thisScore > 0),
                {
                  targetPopulation,
                  crossoverRate: 0.5, // Roughly half the chance to combine.
                  verbose,
                },
              ).then((capsules) =>
                capsules.map((capsule) => {
                  capsule.mutationRate = mutationRate;
                  return capsule;
                })
              );

              brainCapsules.push(...offsprings);
            }
          }

          if (brainCapsules.length < maxPopulation) {
            const clones = equalityCloning(
              seeds,
              maxPopulation - brainCapsules.length,
            ).map((capsule) => {
              capsule.mutationRate = mutationRate;
              return capsule;
            });

            brainCapsules.push(...clones);
          }

          while (brainCapsules.length < maxPopulation) {
            brainCapsules.push(undefined);
          }

          assert(
            brainCapsules.length === maxPopulation,
            `Expected population size ${maxPopulation}, got ${brainCapsules.length}.`,
          );

          if (generation % reportingInterval === 0) {
            console.debug(
              `\r👪 ${
                colors.underline(
                  generation.toString().padStart(
                    countDigits(maxGeneration),
                    " ",
                  ),
                )
              } (${
                Math.round(
                  parseFloat((generation / maxGeneration).toFixed(2)) * 100,
                )
                  .toString()
                  .padStart(2, " ")
              }%): 🤖${brainCapsules.length}/${maxPopulation} 🥇${+overallBest
                .toFixed(2)}${
                fitnessThreshold ? `>=${fitnessThreshold}` : ""
              } (🌱${seeds.length} 🎲${mutationRate.toFixed(2)})    `,
            );
          }
        } else {
          if (generation % reportingInterval === 0) {
            console.debug(
              `\r👪 ${
                colors.underline(
                  generation.toString().padStart(
                    countDigits(maxGeneration),
                    " ",
                  ),
                )
              } (${
                Math.round(
                  parseFloat((generation / maxGeneration).toFixed(2)) * 100,
                )
                  .toString()
                  .padStart(2, " ")
              }%): 🤖${maxPopulation} 💀${maxPopulation} 🥇${+overallBest.toFixed(
                2,
              )} (🌱0 🎲-)    `,
            );
          }
        }

        brains.clear();

        // Fill rounding errors with random brains.
        if (brainCapsules.length < maxPopulation) {
          brainCapsules.push(
            ...new Array(maxPopulation - brainCapsules.length).fill(undefined),
          );
        }

        assert(
          brainCapsules.length === maxPopulation,
          `Expected ${maxPopulation} 🧠💊, got ${brainCapsules.length}.`,
        );

        // Match opponents with similar ELO
        matchPlayers(brainCapsules).map(([brain1, brain2], seat) => {
          pool.enqueue({
            name: "ChessGame",
            payload: {
              brain1,
              brain2,
              seat,
              verbose,
            },
          });
        });
      }
    },
  });

  return pool;
};
