import { assert } from "assert";
import { BrainCapsule } from "../../../lib/BrainCapsule.ts";
import { ChessBrian } from "../../../lib/brians/ChessBrian.ts";
import { autoId } from "../../../lib/FrozenBrain.ts";

const SANE_MAX = 9999;

export type MatingOptions = {
  targetPopulation: number;
  crossoverRate?: number;
  verbose?: number;
};

/**
 * Brians swing until offsprings meet the desired population.
 *
 * Two Brians are chosen with equal probability for mating each time.
 */
export const equalityMating = async (
  brians: BrainCapsule[],
  { targetPopulation, crossoverRate = Math.random(), verbose = 0 }:
    MatingOptions,
) => {
  assert(targetPopulation > 0, "targetPopulation must be greater than 0");
  assert(
    crossoverRate >= 0 && crossoverRate <= 1,
    "crossoverRate must be between 0 and 1.",
  );

  const offsprings = new Set<BrainCapsule>();

  let iteration = 0;
  while (offsprings.size < targetPopulation) {
    if (iteration++ > SANE_MAX) {
      throw new Error(`Potential infinite loop detected, terminating.`);
    }

    const perfHash = autoId(4);
    if (verbose >= 2) console.time(`[mat] ${perfHash}`);

    const sireBrain = pickRandom(brians);
    let damBrain: BrainCapsule;
    while ((damBrain = pickRandom(brians)) === sireBrain);

    const [sire, dam] = await Promise.all([
      ChessBrian.decapsulate(sireBrain),
      ChessBrian.decapsulate(damBrain),
    ]);

    const offspring = await sire.mate(dam, crossoverRate);

    offsprings.add(await offspring.encapsulate());

    sire.dispose();
    dam.dispose();
    offspring.dispose();

    if (verbose >= 2) console.timeLog(`[mat] ${perfHash}`);
  }

  return [...offsprings];
};

const pickRandom = <T>(array: T[]) =>
  array[Math.floor(Math.random() * array.length)];
