import { BrainCapsule } from "../../../lib/BrainCapsule.ts";

/**
 * Equality cloning, all seeds have equal chance to be cloned.
 */
export const equalityCloning = (seeds: BrainCapsule[], population: number) => {
  const breedCount = Math.max(1, Math.floor(population / seeds.length));
  const clones: BrainCapsule[] = [];

  // Clone and package brains for workers to breed.
  for (const seed of seeds) {
    for (let i = 0; i < breedCount && clones.length <= population; i++) {
      const newWeights = new ArrayBuffer(seed.weights.byteLength);
      new Uint8Array(newWeights).set(new Uint8Array(seed.weights));

      clones.push({
        ...seed,
        bestScore: 0,
        weights: newWeights,
      });
    }
  }

  return clones;
};
