import { assert } from "assert";
import { prettyBytes } from "pretty_bytes";
import { writeAll } from "stream";
import { createConnection } from "../../lib/database/postgres.ts";
import { FrozenBrain } from "../../lib/FrozenBrain.ts";

export const uploadFrozenBrains = async (brains: FrozenBrain[]) => {
  const client = await createConnection();

  let totalRowCount = 0;

  for (const { id, bestScore, model, weights } of brains) {
    console.debug("Uploading", prettyBytes(weights.byteLength));
    const { rowCount = 0 } = await client.queryObject(
      `
        INSERT INTO frozen_brain
          (id, timestamp, "bestScore", model)
        VALUES
          ($id, CURRENT_TIMESTAMP(), $bestScore, $model)
        ON CONFLICT (id)
        DO UPDATE SET
          timestamp = CURRENT_TIMESTAMP(),
          "bestScore" = $bestScore
      `,
      { id, bestScore, model },
    );

    try {
      const fd = await Deno.open(`./storage/${id}.bin`, {
        createNew: true,
        write: true,
      });
      writeAll(fd, weights);
      fd.close();
    } catch {
      // noop
    }

    assert(rowCount === 1, `Expected 1 row updated, got ${rowCount}`);

    totalRowCount += rowCount;
  }

  return totalRowCount;
};
