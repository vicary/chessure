import { Contestant } from "../../lib/Contestant.ts";

export const verboseSeeds = (population: Contestant[]) =>
  population
    .filter((b) => b.thisScore || b.bestScore)
    .sort((a, b) => (b.thisScore ?? 0) - (a.thisScore ?? 0) || (b.bestScore ?? 0) - (a.bestScore ?? 0))
    // .slice(0, 10)
    .map((b) => `${`${parseFloat((b?.thisScore ?? 0).toFixed(2))}/${parseFloat((b.bestScore ?? 0).toFixed(2))}`}`)
    .join(", ");
