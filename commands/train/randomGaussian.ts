export const randomGaussian = (min = 0, max = 1): number => {
  let u = 0,
    v = 0;
  while (u === 0) u = Math.random();
  while (v === 0) v = Math.random();
  let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
  num = num / 10.0 + 0.5;
  while (num < 0 || num > 1) num = randomGaussian(min, max);

  return num * (max - min) + min;
};
