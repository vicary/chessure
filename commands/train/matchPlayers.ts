import { Contestant } from "../../lib/Contestant.ts";

/**
 * Match players with similar fitness scores.
 *
 * Only players with fitness within a 10% difference will be matched against
 * each others.
 */
export const matchPlayers = <T extends Contestant | undefined>(
  playersInput: T[],
  eloGap = 0.1,
) => {
  if (playersInput.length % 2 !== 0) {
    throw new Error(
      `How do you match an odd number of players (${playersInput.length}) in a Chess game?`,
    );
  }

  const players = playersInput.slice();
  const matched: [T, T][] = [];
  const unmatched: T[] = [];

  const pickRandomPlayer = (players: T[]) =>
    players.splice(Math.floor(Math.random() * players.length), 1)[0];
  const pickOpponent = (player: T, players: T[]) => {
    const opponentIndexes: number[] = [];
    for (const opponent of players) {
      if (
        Math.abs((player?.thisScore ?? 0) - (opponent?.thisScore ?? 0)) < eloGap
      ) {
        opponentIndexes.push(players.indexOf(opponent));
      }
    }

    const opponents = players.splice(
      opponentIndexes[Math.floor(Math.random() * opponentIndexes.length)],
      1,
    );

    if (opponents.length > 0) {
      return opponents[0];
    } else {
      return null;
    }
  };

  while (players.length > 0) {
    const player = pickRandomPlayer(players);
    const opponent = pickOpponent(player, players);

    if (opponent !== null) {
      matched.push([player, opponent]);
    } else {
      unmatched.push(player);
    }
  }

  // console.debug(`
  //   Elo gap: ${eloGap}
  //   Matched pairs: ${matched.map(([a, b]) => `[${a?.thisScore ?? 0}, ${b?.thisScore ?? 0}]`).join(", ")}
  //   Unmatched players: ${unmatched.map((p) => p?.thisScore ?? 0).join(", ")}
  // `);

  if (unmatched.length > 0) {
    if (eloGap > 0.5) {
      throw new Error();
    }

    matched.push(...matchPlayers(unmatched, eloGap * (1 + Math.E / 10)));
  }

  return matched;
};
