import { assertEquals } from "assert";
import { matchPlayers } from "./matchPlayers.ts";

Deno.test("should match players with similar fitness scores", () => {
  // Generate 10 players with random scores
  const players = Array.from({ length: 10 }, () => ({
    fitness: Math.round(Math.random() * 50),
    bestScore: 0,
    thisScore: 0,
    // deno-lint-ignore require-await
    async clone() {
      return this;
    },
  }));
  const pairs = matchPlayers(players);

  assertEquals(pairs.length, 5);
});
