import { assert } from "assert";
import type { Move } from "chess.js";
import { emojiPiece } from "./emojiPiece.ts";

export const emojiFlags = (
  { color, flags, promotion }: Pick<Move, "color" | "flags" | "promotion">,
) =>
  Array.from(flags).reduce((result, flag) => {
    if (flag === "b") {
      result += "⏩";
    } else if (flag === "e") {
      result += "🏹";
    } else if (flag === "c") {
      result += "🗡";
    } else if (flag === "p") {
      assert(promotion);
      result += "🔼" + emojiPiece(color, promotion);
    } else if (flag === "k") {
      result += "🏰" + emojiPiece(color, "k");
    } else if (flag === "q") {
      result += "🏰" + emojiPiece(color, "q");
    }

    return result;
  }, "");
