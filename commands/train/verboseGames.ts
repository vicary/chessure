import type { ChessInstance } from "chess.js";
import Table from "ascii_table";
import { Chess } from "chess.js";
import * as colors from "colors";

const ChessConstructor = Chess as any as { new (fen?: string): ChessInstance };

export const verboseGames = (games: Map<number, string[]>) => {
  const table = new Table();

  table.setHeading("Seat", "Moves");

  for (const [seat, pgns] of games) {
    pgns
      .map((pgn) => {
        if (!pgn) return;

        const chess = new ChessConstructor();
        if (!chess.load_pgn(pgn, { newline_char: "\n", sloppy: true })) {
          return colors.red("Invalid PGN");
        } else {
          return chess
            .history({ verbose: true })
            .map(({ san }) => san)
            .join(", ");
        }
      })
      .map((moves, index) => [index + 1, moves] as const)
      .forEach(([index, moves]) => {
        if (moves) {
          table.addRow(`${`${seat + 1}`.padStart(2, "0")}-${index}`, moves);
        }
      });
  }

  table.sortColumn(0, (a: string, b: string) => a.localeCompare(b));

  return table;
};
