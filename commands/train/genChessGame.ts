import type { ChessInstance, Move, PieceColor } from "chess.js";
import { Chess } from "chess.js";
import * as colors from "colors";
import type { ChessBrian } from "../../lib/brians/ChessBrian.ts";

export type ChessGame = {
  game?: ChessInstance;
  black: ChessBrian;
  white: ChessBrian;
  verbose?: boolean;
};

export type ChessGameMove = Omit<Move, "color" | "piece"> & {
  color?: Move["color"];
  piece?: Move["piece"];
  opponent: ChessBrian;
  player: ChessBrian;
  side: PieceColor;
  turn: number;
};

/**
 * Custom flags on top of chess.js:
 * - "I" - Illegal move
 * - "C" - Check
 * - "M" - Checkmate
 * - "S" - Stalemate
 * - "T" - Threefold repetition
 * - ~~"D" - Fifty-move draw~~
 * - ~~"R" - Resignation~~
 * - ~~"O" - Timeout~~
 * - ~~"E" - Error~~
 */
export async function* genChessGame({
  black,
  white,
  game = new Chess(),
  verbose = false,
}: ChessGame): AsyncGenerator<ChessGameMove> {
  let halfTurns = 0;

  while (!game.game_over()) {
    const turn = Math.floor(halfTurns++ / 2);
    const side = game.turn();
    const [player, opponent] = side === "w" ? [white, black] : [black, white];
    const moveAttempt = await player.think(game);
    const moveResult: ChessGameMove = {
      from: moveAttempt.from,
      to: moveAttempt.to,
      flags: "I",
      san: "",
      opponent,
      player,
      side,
      turn,
      ...game.move(moveAttempt),
    };

    if (game.in_check()) {
      moveResult.flags += "C";
    }

    if (moveResult.flags.includes("I")) {
      if (verbose) {
        console.debug(
          colors.dim(
            colors.strikethrough(
              `${player.name}: ${moveAttempt.from} -> ${moveAttempt.to}`,
            ),
          ),
        );
      }

      const piece = game.get(moveAttempt.from);

      // if (moveAttempt.from !== moveAttempt.to) {
      //   game.remove(moveAttempt.from);
      //   if (piece) {
      //     game.put(piece, moveAttempt.to);
      //   }
      // }

      moveResult.color = piece?.color;
      moveResult.piece = piece?.type;

      yield moveResult;

      return;
    } else {
      if (verbose) {
        console.debug(
          `${player.name}: ${moveAttempt.from} -> ${moveAttempt.to}`,
        );
      }

      if (game.game_over()) {
        if (game.in_checkmate()) {
          moveResult.flags += "M";
        } else if (game.in_stalemate()) {
          moveResult.flags += "S";
        } else if (game.in_threefold_repetition()) {
          moveResult.flags += "T";
        }
      }

      if (turn > 2) {
        console.debug(colors.red(`Turn ${turn}!`));
      }

      yield moveResult;
    }
  }
}
