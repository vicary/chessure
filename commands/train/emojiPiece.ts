import type { PieceColor, PieceType } from "chess.js";

const emojiPieces = Object.freeze({
  wp: "♙",
  wn: "♘",
  wb: "♗",
  wr: "♖",
  wq: "♕",
  wk: "♔",
  bp: "♟",
  bn: "♞",
  bb: "♝",
  br: "♜",
  bq: "♛",
  bk: "♚",
});

export const emojiPiece = (color: PieceColor, piece: PieceType) =>
  emojiPieces[`${color}${piece}`];
