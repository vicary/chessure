import type { Promisable } from "type-fest";

export type Epoch<T = unknown> = { generation: number; population: T[] };

export async function* genGenerations<T = unknown>(
  onEpoch: (epoch: Epoch<T>) => Promisable<T[]>,
) {
  let population: T[] = [];
  let generation = 0;

  do {
    generation++;

    population = await onEpoch({ generation, population });

    yield { generation, population } as Epoch<T>;
  } while (population.length > 0);
}
