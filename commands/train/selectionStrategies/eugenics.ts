import { Contestant } from "../../../lib/Contestant.ts";

/**
 * Select seeds from given population based on three criteria:
 * 1. global max 5%
 * 2. local max 5%
 * 3. random 5%
 */
export const eugenics = <T extends Contestant>({
  externalSeeds,
  fitnessThreshold,
  population,
  selectionRatio = 0.15,
}: {
  /**
   * The whole population to be selected from.
   */
  population: T[];

  /**
   * The pouplation ratio to get preserved, for each group of selection.
   */
  selectionRatio?: number;

  /**
   * Optional threshold for fitness lower bound.
   */
  fitnessThreshold?: number;

  /**
   * If specified, selection process will include these manually selected contestants.
   *
   * Eugenics happens to the remaining quota after this "priviledged class".
   */
  externalSeeds?: (population: T[]) => T[];
}) => {
  if (selectionRatio < 0 || selectionRatio > 1) {
    throw new Error(`preserveRatio must be between 0 and 1, but got ${selectionRatio}`);
  }

  // Joke: Priviledged class, eugenics don't know why they are there.
  // Fact: Random new seeds for diversity.
  const seeds: T[] = externalSeeds?.(population) ?? [];

  if (seeds.length > population.length) {
    throw new RangeError(`Too many priviledged seeds, you need actual labours to support them.`);
  }

  const selectionCount = Math.max(0, ((population.length - seeds.length) * selectionRatio) / 2);

  // Best of this generation
  const currentBests = population
    .filter((b) => (b.thisScore ?? 0) >= (fitnessThreshold ?? 0))
    .sort((a, b) => (b.thisScore ?? 0) - (a.thisScore ?? 0))
    .slice(0, Math.ceil(selectionCount));

  // Best of all generations
  const overallBests = population
    .filter((b) => fitnessThreshold === undefined || (b.bestScore ?? 0) >= fitnessThreshold)
    .sort((a, b) => (b.bestScore ?? 0) - (a.bestScore ?? 0) || (b.thisScore ?? 0) - (a.thisScore ?? 0))
    .slice(
      0,
      // Greatly favors current git generation.
      currentBests.length > 0 ? 1 : Math.floor(selectionCount),
    );

  seeds.push(...new Set([...currentBests, ...overallBests]));

  return seeds;
};
