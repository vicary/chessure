import { createConnection } from "../../lib/database/postgres.ts";
import { FrozenBrain } from "../../lib/FrozenBrain.ts";

export const downloadBestBrains = async (
  resume: number | string[],
): Promise<FrozenBrain[]> => {
  const connection = await createConnection();

  // console.debug({ resume });

  const { rows } = typeof resume === "number"
    ? await connection.queryObject<FrozenBrain>(
      `
            SELECT * FROM frozen_brain
              WHERE "deletedAt" IS NULL
              ORDER BY "bestScore" DESC, timestamp ASC
              LIMIT $take
          `,
      { take: resume },
    )
    : await connection.queryObject<FrozenBrain>(
      `
            SELECT * FROM frozen_brain
              WHERE "deletedAt" IS NULL
              AND id IN (${resume.map((_, i) => `$${i + 1}`).join(", ")})
              ORDER BY "bestScore" DESC, timestamp ASC
          `,
      resume,
    );

  const brains = await Promise.all(
    rows.map(async ({ id, bestScore, weights, ...data }) => {
      try {
        weights ??= await Deno.readFile(`./storage/${id}.bin`);
      } catch (e) {
        if (e.name === "NotFound") {
          console.debug(`Weight data for Brain#${id} not found, deleting...`);
          await connection.queryObject(
            `DELETE FROM frozen_brain WHERE id = $id`,
            { id },
          );
          return;
        } else {
          throw e;
        }
      }

      return new FrozenBrain({
        ...data,
        id,
        bestScore: Number(bestScore),
        weights,
      });
    }),
  );

  connection.release();

  return brains.some((b) => b === undefined)
    ? downloadBestBrains(resume)
    : (brains as FrozenBrain[]);
};
