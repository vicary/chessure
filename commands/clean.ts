import { assert } from "assert";
import { Command } from "commander";
import { createConnection } from "../lib/database/postgres.ts";

export const clean = async (
  fitness: number,
  { hard, lte }: { hard: boolean; lte: boolean },
) => {
  assert(
    !isNaN(fitness) && fitness >= 0,
    "<fitness> must be a positive number.",
  );
  const connection = await createConnection();

  const where = `WHERE "bestScore" ${lte ? "<=" : "="} $fitness`;
  const { rowCount } = hard
    ? await connection.queryObject(`DELETE FROM frozen_brain ${where}`, {
      fitness,
    })
    : await connection.queryObject(
      `UPDATE frozen_brain SET deletedAt = now() ${where}`,
      { fitness },
    );

  console.info(`🪦 ${rowCount} (🥇${lte ? "≤" : "="}${fitness})`);

  connection.release();
};

const addCleanCommand = (program: Command) => {
  program
    .command("clean")
    .description("Remove stored Brians from the database.")
    .arguments("<fitness>", (value: string) => parseInt(value))
    .option(
      "--hard",
      "Hard delete brains, including those already archived.",
      false,
    )
    .option(
      "--lte",
      "Instead of strict equal, delete brains with a fitness less than or equal to the given value.",
      false,
    )
    .action(clean);
};

export default addCleanCommand;
