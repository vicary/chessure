import { Command } from "commander";
import packageJson from "../deno.json" assert { type: "json" };
import addDebugCommand from "./debug.ts";
import addTrainCommand from "./train.ts";

const program = new Command();

addDebugCommand(program);
addTrainCommand(program);

await program
  .description("Headless ChessBrians trainer with TensorFlow JS.")
  .version(packageJson.version)
  .parseAsync(Deno.args);
