import type { ChessInstance } from "chess.js";
import { Chess } from "chess.js";
import { Command } from "commander";
import { createConnection } from "../lib/database/postgres.ts";

const ChessConstructor = Chess as any as { new (fen?: string): ChessInstance };

export const debug = async (sql = "show tables") => {
  const connection = await createConnection();

  const { rows, rowCount } = await connection.queryObject(sql);

  if (rows.length === 0) {
    console.log({ rowCount });
  } else if (rows.length === 1 && (rows[0] as any).create_statement) {
    const result = rows[0] as any;
    if (result.create_statement) {
      console.log(result.create_statement);
      return;
    }
  }

  console.log(rows);
};

const addDebugCommand = (program: Command) =>
  program.command("debug", { hidden: true }).arguments("[sql]")?.action(debug);

export default addDebugCommand;
