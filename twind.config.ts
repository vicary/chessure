import { Options } from "$fresh/plugins/twind.ts";
import { aspectRatio } from "@twind/aspect-ratio";

export default {
  plugins: {
    aspect: aspectRatio,
  },
  selfURL: import.meta.url,
} as Options;
